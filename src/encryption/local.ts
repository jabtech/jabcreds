import * as crypto from 'crypto';
import { config } from 'jabcore';
import { Encryption } from './encryption';

const ENCRYPTION_KEY = config.get('ENCRYPTION:KEY') || 'cdGe66wugBZEhdkemEWX32jqJRAdhvM6'; // Must be 256 bytes (32 characters)
const IV_LENGTH = 16; // For AES, this is always 16

export class LocalEncryption extends Encryption {

  public static encrypt(data: string): string {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(data);
  
    encrypted = Buffer.concat([encrypted, cipher.final()]);
  
    return iv.toString('hex') + ':' + encrypted.toString('hex');
    }    
    
    public static decrypt(data: string): string {
        let textParts = data.split(':');
        let iv = Buffer.from(textParts.shift()!, 'hex');
        let encryptedText = new Buffer(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
        let decrypted = decipher.update(encryptedText);
      
        decrypted = Buffer.concat([decrypted, decipher.final()]);
      
        return decrypted.toString();
    }

    
}