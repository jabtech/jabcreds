import { Encryption } from './encryption';
export declare class LocalEncryption extends Encryption {
    static encrypt(data: string): string;
    static decrypt(data: string): string;
}
