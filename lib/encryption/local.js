"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const jabcore_1 = require("jabcore");
const encryption_1 = require("./encryption");
const ENCRYPTION_KEY = jabcore_1.config.get('ENCRYPTION:KEY') || 'cdGe66wugBZEhdkemEWX32jqJRAdhvM6';
const IV_LENGTH = 16;
class LocalEncryption extends encryption_1.Encryption {
    static encrypt(data) {
        let iv = crypto.randomBytes(IV_LENGTH);
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
        let encrypted = cipher.update(data);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return iv.toString('hex') + ':' + encrypted.toString('hex');
    }
    static decrypt(data) {
        let textParts = data.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = new Buffer(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
}
exports.LocalEncryption = LocalEncryption;
//# sourceMappingURL=local.js.map