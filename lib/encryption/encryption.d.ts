export declare abstract class Encryption {
    static encrypt(data: string): string;
    static decrypt(data: string): string;
}
